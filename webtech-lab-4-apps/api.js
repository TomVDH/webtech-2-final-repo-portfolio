(function () {
    'use strict';
    /*global $, jQuery, console, alert */
    var latitude, longitude, url, Location, myLocation, date, dateTime, day, days, month, months, year, years, getTest;

    Location = function () {
        this.getLocation = function () {
            if (navigator.geolocation) {
                console.log('Geolocation OK');
                navigator.geolocation.getCurrentPosition(this.updateWeather);
            } else {
                console.log('Geolocation niet OK');
            }
        };

        this.updateWeather = function (pos) {
            console.log("Position OK!");
            console.log(pos.coords.latitude);
            console.log(pos.coords.longitude);

            latitude = pos.coords.latitude;
            longitude = pos.coords.longitude;


            url = "https://api.forecast.io/forecast/412e0bc4cc3095a2de7d0bdf663f4e3e/" + latitude + "," + longitude + "?units=si";

            $.ajax({
                url: url,
                dataType: 'jsonp',
                type: 'GET',
                success: function (resp) {
                    console.log(resp);
                    document.getElementById("tempAvg").innerHTML = Math.round(resp.hourly.data[24].temperature) + "°C";
                    document.getElementById("tempMin").innerHTML = "Minimum temperature: " + Math.round(resp.daily.data[1].temperatureMin) + "°C";
                    document.getElementById("tempMax").innerHTML = "Maximum temperature: " + Math.round(resp.daily.data[1].temperatureMax) + "°C";
                    document.getElementById("summary").innerHTML = resp.hourly.data[24].summary;
                    document.getElementById("picto").setAttribute("src", "img/" + resp.daily.data[1].icon + ".png");
                    date = new Date();
                    getTest = date.getDate();
                    day = date.getDay();
                    days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                    month = date.getMonth();
                    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "Novmeber", "December"];
                    year = date.getFullYear();
                    date = days[day + 0] + ", " + (getTest + 0) + " " + months[month] + " " + year;
                    document.getElementById("dateBx").innerHTML = date;
                    
                }
            });

        };
    };

    myLocation = new Location();
    myLocation.getLocation();

}());