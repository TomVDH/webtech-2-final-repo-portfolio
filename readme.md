# Webtech 2 #

Alle repo's vind je op Bitbucket, niet alle repos hebben data: https://bitbucket.org/TomVDH


## Lab 1 - GIT ##

-[https://bitbucket.org/TomVDH/webtech-lab-1]

### Kleine site met gerechten maken, per 4, ieder één pagina en deze tesamen collaboraten naar Git. Ik zat alleen, heb deze site en opdracht dus alleen gemaakt. ###

Uitsluitend gewerkt met de GitHub for Windows GUI tool. Nu ben ik overgestapt op SourceTree.

Het hele principe achter de master branch met de vertakkende development branches kwam ook aan bod in de les. Heb dit echter niet gebruikt tot mijn overstap naar SourceTree, waar er krachtigere en beter werkende functies voor zijn. Dit geldt ook voor push en pull, dat in de GitHub for Windows Client niet apart bestaat.

Forking en Pull requests waren snel duidelijk, forken betekent eigenlijk kopiëren en plakken in je eigen Git account, terwijl een pull request de eigenaar van een Git Repo de kans geeft om je code te vergelijken en in de finale repo te steken.

Tegenwoordig werk ik het vaakst met BitBucket, dit omdat ik bijna 3 maand heb moeten wachten op mijn GitHub for Students pack, uit gewoonte gebruik ik dus BitBucket, dat ook mijn voorkeur geniet qua snelheid en layouting op de site.

## Lab 2 - CSS Animations ##

Gëavanceerde CSS animaties maken.

Twee belangrijke items binnen CSS animatie: Transitions en Transformations, met elk hun grote verschillen.

### Transitions ###
Is een lineaire overgang van iets naar iets anders (positie X naar positie X + 5) en kan enkel uitgevoerd worden door een CSS pseudo-class zoals :hover. Kan niet zichzelf invoken.

### Transformation ###
Transformaties zijn veelzijdiger in wat ze kunnen doen. Ze kunnen zelf starten, en werken beter en efficienter (beter performance).

Je hebt ook twee onderverdelingen: 2D en 3D.

#### 2D Transformations ####
Je hebt de volgende "operators" voor 2D tranformaties..

- Translate(x,y): lineaire beweging links rechts (x+/-) en verticaal (y+/-) of diagonaal (x+/- en y+/-).

- Rotate(angle): roteren rond de centrale as van het object, de rotatiegraad druk je uit in graden (°).

- Scale(x, y): Grootte bepalen met animaties. Je kan vergoten en verkleinen. x = breedte, y = hoogte.

- Skew(x,y): Kantelen van element rond de x en y-as. X is horizontaal, en Y is verticaal.

#### 3D Transformations ####
De meeste operators zijn hetzelfde voor 3D transformations, je hebt echter wel een z-as.

- RotateX, RotateY, RotateZ: Vanzelfsprekend, roteren volgens x-as, y-as of z-as.

- Perspective: is een unieke operator voor 3D transformations en beinvloedt de virtuele "afstand" of "positie" van het element tov de gebruiker en vice versa.


### Animations in CSS ###
Animations zijn een derde methode van animeren binnen CSS en meteen de veelzijdigste maar moeilijkst om op te stellen.

- Animations gebruiken keyframes, zoals After Effects

- Kunnnen uit zichzelf starten 

- Kunnen loopen


## Lab 3 - Eigen JavaScript Framework (advanced JS) ##

We kregen de grondsbeginselen van een custom mini-js framework, waarmee we een ToDo app moesten bouwen. Enkel JavaScript, geen jQuery.

### Opzet ###
jQuery neemt bandbreedte en laadtijd in, soms onnodig, omdat een klein en eenvoudig mini Js framework de meeste taken op zich kan nemen, en deze sneller en efficienter kan uitvoeren ipv de hele "logge" jQuery library in te laden. Interessant voor mobiele applicaties.

### Wat geleerd? ###
De data object structuur binnen JS werd opgefrist. Alle object-datatypes (boolean, number, string, datum, array, function) en hoe men deze opbouwt en gebruikt (var Koekendoos = new Object()).

## Lab 4 - Building an app with APIs ##

Een API is een bibliotheek die data bevat of data ophaalt en kan gebruikt worden voor het leveren van content binnen een webpagina of applicatie. Maps, weersvoorspellingen en RSS-lijsten kunnen APIs zijn en kan men in apps inwerken.

Deze les hebben we Geolocation gebruikt, voor onze weerapp die we moesten gebruiken.

### navigator.geolocation.getCurrentPosition() ###
Dit is een functie binnen native HTML5, die ons toelaat de coordinaat gegevens op te halen van de locatie van de gebruiker. In het geval van deze les, hebben we de coordinaat gegevens binnen onze code toegevoegd aan de URL (de template van forecast.io gebruikt coordinaat gegevens in de URL om de locatie op te vragen en share-baar te maken).

### Local Storage ###
APIs zijn vaak betalend maar toch zijn er ook heel wat gratis versies te gebruiken, deze hebben echter een opvraaglimiet van 1000 per dag. Dus je app de gratis API gebruikt, en alle gebruikers die je hebt blijven refreshen (omdat je geen gegevens locaal cachet) dan zit je snel aan je limiet, en moet je een dag wachten tot je app terug werkt. Dit spaart zowel op je nog te gebruiken refreshes alsook de dataverzending van de persoon die de app gebruikt. 

### AJAX ###

Lokaal en instant refreshen zonder de hele pagina opnieuw te laden gaat via AJAX, waar we die les ook voor 't eerste mee bezig waren. Het kan nieuwe data ophalen, snel en bijna instant, terwijl de rest van de pagina verder blijft functioneren. Er moet niet gereload, of gerefreshed worden zodat AJAX kan werken.

### JSON files ###
JSON is de object notatie van JavaScript en wordt veel gebruikt binnen frameworks zoals Node en Express. Voor de API-les gebruikten we een variant JSONP (P = padding). Deze file geeft aan waar de API te vinden is, welk datatype we wensen op te halen en wat men moet doen indien alles is ingeladen. Essentieel dus. JSONP heeft niet altijd volledige functionaliteit: oude browsers en mobiele browsers werken er niet altijd mee samen(?).


## Lab 5 - Realtime apps met Node.js en web sockets ##

Deze les bestaat in principe uit twee lessen: een gastles door District01 en een opfrissingsles met onze docent zelf.

Alles wat ik over dit onderwerp weet komt door bijles en deels door de opfrissingsles van onze docent. De gastles was veruit onvoldoende en heb daar toen niets uit opgestoken.

### Node.js ### 
Node JS is een JS-framework dat toestaat om asynchroon applicaties te ontwikkelen en uit te voeren. Node.JS draait niet binnen de browser zoals JS normaal doet, maar is een framework dat Server side geinstalleerd staat en daar werkt. Zo kan Node in principe op alle platformen en devices werken.

### Groot voordeel Node Js ###
Node.js is populair, al meteen een groot voordeel, maar misschien het belangrijkste vanuit developer-perspectief is de ingebouwde webserver capaciteit. Wanneer je developed, kan Node.js zelf een lokale server opstarten en draaien om je apps te testen. Dit gaat allemaal via de command line, en is na gewenning, een krachtige en goed werkende tool om je Node.js applicatie verder uit te bouwen.

### Web Sockets ###
Web sockets zijn een onderdeel van Node.js en staan rechtstreeks real-time data verkeer tussen 2 "sockets" toe. Streamen, eigenlijk,  omdat het realtime en tegelijkertijd kan. Dit is in contrast met traditioneel browser-verkeer dat 1-way is. Je moet altijd een "call-to-action" van de user hebben om de browser iets te laten doen. Websockets staan altijd paraat en kunnen geconfigureerd worden van autonoom dingen te doen. (push methode)

### Installatie en gebruik van Node.js + mongoDB ###
De database voor onze Node.js apps is MongoDB, een lichte, door de commandline gestuurde database structuur. Wel moeilijk, maar ze werkt goed.

Node.JS moet je lokaal wel eerst installeren (of op je webserver). Installen gebeurt via de commandline en het verdere gebruik ook.

Node.JS heeft heel wat modules, die het maken van je applicatie gemakkelijker maken, zoals express, sockets.io en nodemon. Dit zijn 3 plugins die ik vast gebruik wanneer ik met node.js ga prutsen.

Installeren doe je met: npm install express -g

Dit installeert het express framework (maakt alle dependencies en submappen netjes aan) als een global. Dit wil zeggen dat de hele node applicatie de express structuur kan gebruiken (?).

### Package.json en Jade ###
In de Package.json vinden we de module dependencies terug. express kan dit genereren. Je kan het ook handmatig maken. Als je dit niet hebt kan je de app niet draaien.

Jade is een template-engine die je toestaat HTML in een "verkorte" versie te schrijven. Indenting is er niet echt, maar je kan het toch door tabs of spaties te gebruiken. Je kan deze wel niet mixen, want dan geeft Jade een fout. Ik verkies pure HTML boven Jade, want de laatste heeft ook geen systeem voor het afsluiten van brackets (</>).

### Nodemon ###
Node.js apps draaien als een "momentopname". Wanneer je iets in de code wijzigt terwijl de app draait, zal de app niet real-time updaten, je moet ze dus herstarten. Normaal moet je dit handmatig doen door het commando "rs" in te voeren. Dit is echter traag en vervelend, want na elke dit moet je de console terug opendoen om rs in te voeren.

Nodemon is een node.js plugin die je project automatisch opnieuw opstart wanneer er een wijziging in de bestandstructuur wordt gevonden.

## Lab 6 - Angular.js ##

Angular is een MVC-js framework uitgebaat door Google dat we kunnen gebruiken voor het maken van krachtige, repsonsieve applicaties. SPA (single page applications) is de strong suit van Angular.

De opbouw van de code is echter relatief eenvoudig.

De angular notatie binnen de code is "ng".

"ng-app": dit is de hoogst mogelijke level voor je applicatie, het root element, dus. Je hebt dit nodig om directives en data-koppelingen te gebruiken.

"ng-controller": dit is een tweede belangrijke angular-tag: de controller koppelt een JS controller aan de view, dit is nodig voor de functionele logica van de app te kunnen runnen.


## Lab 7 - SASS ##

SASS is een "extensie" van CSS, deze werkt op Ruby, dus dat moet je eerst installeren. Om SASS te installeren, moeten we de Ruby prompt gebruiken en enkele install commands ingeven.

SASS heeft een mappenstructuur, en we moeten ook sommige files effectief gaan includen (zoals in PHP), dit zijn de files met _ (underscores) in de bestandsnaam.

SASS files moeten gecompileerd worden naar gewone CSS, dit gaat ook via de command terminal (sass --watch)

### Variabelen ###

SASS maakt gebruik van "variabelen" zoals in Javascript. Je kan een kleur dus kopplen aan $kleur1 en deze later in de css ook gebruiken.

$kleur1: 00000f;

.div
{
	color: $kleur1;
	
}

### Andere SASS-eigenschappen ###
SASS heeft iets dat Mixins heet. Met Mixins kan je CSS waarden/eigenschappen maken, en deze gebruiken voor andere items of sites.

SASS gebruikt ook de @Extend paramter. Hiermee kan je CSS eigenschappen van één element, overdragen naar de andere.


## Lab 8 - GULP ##
GULP is een tool die je kan runnen binnen je node.

Het is voornamelijk bedoeld om lastige en repetitieve taken binnen je development over te nemen.

De voornaamste taken zijn: code minifyen, images compressen.