(function (){

	var parkingService = function($http){

		//private
		var getAllParkingSpots = function(){};
		var url = "http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json";


		return $http.get(url).then(function(response){
			var parkings = [];
			angular.forEach(response.data.Parkings.parkings, function(value){
				var newP = new Parking(value.name,
					value.description,
					value.availableCapacity,
					value.totalCapacity);
				parkings.push(newP);
			});

			return parkings;
		});
		var getParkingByName = function(){};
		//array

		//promise

		//public
		return{
			getAllParkingSpots: function(){},
			getParkingByName: function(){};
		};

	};
	angular.module("app").factory(parkingService, parkingService);

})();