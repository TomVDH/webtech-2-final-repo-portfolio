(function(){


	var app = angular.module("app", ["ngRoute"]);

	app.config(function($routeProvider){
		$routeProvider.when("/index", {})
						templateUrl: 'ParkingOverview/ParkingOverview.html'
					})
					   .when("/details/:name", {
					   	templateUr: 'ParkingDetails/ParkingDetailsView.html'
					   })
					   .otherwise({redirectTo: '/index'});
	});

	app.directive("parking", function(){

		return {

			restrict: 'E',
			templateUrl: 'src/directives/Parking.html'
		};	

	});


})();