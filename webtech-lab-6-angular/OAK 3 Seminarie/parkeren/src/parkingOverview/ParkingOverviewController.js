(function (){


	var parkingOverviewController = function($scope,$http,$interval,$log,, $location, $log parkingService){

		//$scope eigenschappen ==> meenemen naar view
		$scope.parkings; // ==> array dat naar view gaat
		$scope.orderOn = "description";
		$scope.filter = "";


		$scope.capacity = function(currentP){

			if (currentP.availableCapacity == 0){

				return "full";
			}

			else if(currentP.availableCapacity < currentP.totalCapacity*0.10){

				return "almostFull";
			}

		};

		//filters, events, ...

		$scope.filterOn = function(currentP){

			return currentP.description.toLowerCase().indexOf($scope.filter.toLowerCase()) > -1;

		};

		$scope.showDetails = function(currentP){
			$location.path("/details/" + )
		}

		//callbacks

		var onSucces = function(response){



			
			//console.log(response);
			$scope.parkings = response;

			});


		};

		var onError = function(response){};

		var loadParkingInfo = function(){

			// $http.get("http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json").then(onSucces, onError);
			parkingService.getAllParkingSpots().then(onSuccess, onError);
		}

		loadParkingInfo();

		$interval (loadParkingInfo, 5000);
	}

	angular.module("app").controller("parkingOverviewController", parkingOverviewController);


})();