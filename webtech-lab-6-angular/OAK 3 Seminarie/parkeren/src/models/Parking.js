function Parking (name, description, availableCapacity, totalCapacity) {

	this.name = name;
	this.description = description;
	this.availableCapacity = availableCapacity || 0;
	this.totalCapacity = totalCapacity;

}

Parking.prototype = {

	toString: function(){

		return this.description;
	}
}